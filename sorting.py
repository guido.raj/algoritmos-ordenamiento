#!/usr/bin/env python
#encoding: utf-8

# Ordena el arreglo in-place.
def selection_sort(arr):
    print '//////////---Selection Sort---//////////'
    # Invariante: arr[0, i] está ordenado y son los mínimos.
    for i in xrange(0, len(arr)):
        print 'Iteracion: ', i+1
        # busco el próximo elemento más chico y lo ubico adelante.
        i_min = min_index_from(i, arr)
        arr[i], arr[i_min] = arr[i_min], arr[i]
        print 'Orden hasta ahora '+str(arr[0:i+1])+'|'+str(arr[i+1:len(arr)])
    return arr

def insertion_sort(arr):
    print '//////////---Selection Sort---//////////'
    # Invariante: arr[0, i] está ordenado.
    for i in xrange(0, len(arr)):
        print 'Iteracion: ', i+1
        # Inserto el próximo elemento de forma ordenada.
        j, k = i-1, i
        print 'La variable i:', i
        while (j >= 0 and arr[j] > arr[k]):
            print 'La variable j:', j
            print 'La variable k:', k
            arr[j], arr[k] = arr[k], arr[j]
            k, j = j, j-1
            print 'Orden hasta ahora '+str(arr[0:i+1])+'|'+str(arr[i+1:len(arr)])
    return arr


def bubble_sort(arr):
    must_check_array = True
    # Hasta que no haya swaps tengo que revisar el arreglo.
    while (must_check_array):
        must_check_array = False
        for i in xrange(0, len(arr) - 1):
            if arr[i] > arr[i+1]:
                arr[i], arr[i+1] = arr[i+1], arr[i]
                must_check_array = True


# Funciones Auxiliares
# Devuelve el índice del primer elemento más chico de arr[from_i, len(arr)].
# PRE: 0 <= from_i < len(arr)
def min_index_from(from_i, arr):
    min_i = from_i
    for j in xrange(from_i, len(arr)):
        if (arr[j] < arr[min_i]):
            min_i = j

    return min_i

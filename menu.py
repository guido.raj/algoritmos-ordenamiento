#!/usr/bin/env python
#encoding: utf-8
from sorting import (
    selection_sort,
    insertion_sort,
    bubble_sort
    )

algorithms = {1:bubble_sort, 2:insertion_sort, 3:selection_sort}

def menu():
    print '//////////---MENU---//////////'
    print '¿Que algoritmo desde usar?'
    print '1) Bubble Sort'
    print '2) Insertion Sort'
    print '3) Selection Sort'
    option = int(raw_input('Opcion:'))
    res = algorithms.get(option)([5,4,3,2,1])
    print 'Resultado: ', res

def select_run():
    pass

menu()
